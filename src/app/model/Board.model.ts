import { List } from './List.model';

export class Board{
  constructor(
    public id?:string,
    public name?:string,
    public created_date?:Date,
    public lists?:List[]
  ){}
}
