import { Tag } from './Tag.model';
import { User } from './User.model';

export class Attendee{
  constructor(
    public id?:string,
    public user?:User,
    public card?:Tag
  ){}
}
