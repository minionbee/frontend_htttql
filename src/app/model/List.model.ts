import { Tag } from './Tag.model';
import { Board } from './Board.model';

export class List{
  constructor(
    public id?:string,
    public name?:string,
    public board?:Board,
    public cardDTOS?:Tag[]
  ){}
}
