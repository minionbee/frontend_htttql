import { Timestamp } from 'rxjs';
import { Tag } from './Tag.model';

export class Debate{
  constructor(
    public id?:number,
    public content?:string,
    public employee?:string,
    public time?:Date,
    public card?:Tag
  ){}
}
