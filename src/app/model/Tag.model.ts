import { User } from './User.model';
import { List } from './List.model';
import { Detail } from './Detail.model';
import { Attendee } from './Attendee.modal';

export class Tag{
  constructor(
    public id?:string,
    public status?:string,
    public name?:string,
    public label?:string,
    public isort?:number,
    public listBoard?:List,
    public start_date?:Date,
    public end_date?:Date,
    public attendees?:Attendee[],
    public checkList?:Detail[],
    public qty?:number,
    public percent_complete?:string,
    public doing_complete_expire?:number
  ){}
}
