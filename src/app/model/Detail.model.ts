import { Tag } from './Tag.model';

export class Detail{
  constructor(
    public id?:number,
    public status?:string,
    public name?:string,
    public tag?:Tag,
    public checklist_time?:Date
    // public type_list_of_tag?:string
  ){}

}
