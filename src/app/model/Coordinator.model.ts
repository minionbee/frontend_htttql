import { User } from './User.model';
import { Board } from './Board.model';

export class Coordinator{
  constructor(
    public id?:string,
    public role?:string,
    public userDTO?:User,
    public boardDTO?:Board
  ){}
}
