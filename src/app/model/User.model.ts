import { Department } from './Department.model';

export class User{
  constructor(
    public username?:string,
    public password?:string,
    public email?:string,
    public phone_number?:string,
    public full_name?:string,
    public address?:string,
    public role?:string,
    public id?:string,
    public selected_user_in_created_card?:number,
    public is_user_in_board?:number,
    public department?:Department
  ){}
}
