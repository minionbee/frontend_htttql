import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn:"root"
})

export class TagService{
  constructor(private httpClient:HttpClient){}

  addNewTag(list_id,user_id,name,label,start_date,end_date){
    // return this.httpClient.post<any>("http://localhost:8080/api/tag/"+board_id+"/"+list_id+"/"+user_id,{name,label,start_date,end_date});
    return this.httpClient.post<any>("http://localhost:8080/api/tag/"+list_id+"/"+user_id,{name,label,start_date,end_date});
  }
  updateUserToTag(card_id,is_add,user_id){
    return this.httpClient.put<any>("http://localhost:8080/api/tag/"+card_id+"/"+is_add+"/"+user_id,null);
  }

  updateATag(tag_id,user_id,is_add,name,label){
    return this.httpClient.put<any>("http://localhost:8080/api/tag/"+tag_id+"/"+user_id+"/"+is_add,{name,label});
  }
  removeATag(tag_id){
    return this.httpClient.delete<any>("http://localhost:8080/api/tag/"+tag_id);
  }
  moveLocationTag(card_id,list_id,pre_index,new_index){
    return this.httpClient.put<any>("http://localhost:8080/api/card/"+card_id+"/"+list_id+"/"+pre_index+"/"+new_index,null);
  }
  moveTagBetweenTwoList(card_id,pre_list_id,new_list_id,pre_index,new_index){
    return this.httpClient.put<any>("http://localhost:8080/api/card/"+card_id+"/"+pre_list_id+"/"+new_list_id+"/"+pre_index+"/"+new_index,null);
  }
}
