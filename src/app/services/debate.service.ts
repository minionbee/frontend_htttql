import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DebateService {


  constructor(private httpClient:HttpClient) { }

  getAllDebateByCard(card_id){
    return this.httpClient.get<any>("http://localhost:8080/api/debate/"+card_id);
  }

  postAComment(content,time,employee,card_id){
    return this.httpClient.post<any>("http://localhost:8080/api/debate/"+card_id,{content,time,employee});
  }
  deleteAComment(debate_id){
    return this.httpClient.delete<any>("http://localhost:8080/api/debate/"+debate_id);
  }
}
