import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DutyService {

  constructor(private httpClient:HttpClient) { }

  getAllMemberCurrentInABoard(board_id){
    return this.httpClient.get<any>("http://localhost:8080/api/duty/board/"+board_id);
  }
  addANewMemberToBoard(board_id,user_id,role){
    return this.httpClient.post<any>("http://localhost:8080/api/duty/p/"+board_id+"/"+user_id,{role});
  }
  deleteADuty(board_id,user_id){
    return this.httpClient.delete<any>("http://localhost:8080/api/duty/d/"+board_id+"/"+user_id);
  }
}
