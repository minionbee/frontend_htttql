import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ControlRegisterModalService {
  private registerStatusSubject:BehaviorSubject<boolean>=new BehaviorSubject<boolean>(false);
  registerModalStatus$:Observable<boolean>=this.registerStatusSubject.asObservable();
  setRegisterModalStatus(status:boolean){
    this.registerStatusSubject.next(status);
  }
}
