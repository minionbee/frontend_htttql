import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../model/User.model';

@Injectable({
  providedIn:"root"
})

export class UserService{

  membersInTheSameDepartmentSubject:BehaviorSubject<User[]>=new BehaviorSubject<User[]>(null);
  membersInTheSameDepartment$:Observable<any>=this.membersInTheSameDepartmentSubject.asObservable();

  userInEditViewSubject:BehaviorSubject<User>=new BehaviorSubject<User>(null);

  constructor(private httpClient:HttpClient){}

  getUserByUsername(username:string){
    return this.httpClient.get<any>("http://localhost:8080/api/user/"+username);
  }
  getAllMemberInTheSameDepartment(leader_id){
    return this.httpClient.get<any>("http://localhost:8080/api/department/member/"+leader_id);
  }
  getAllUser(){
    return this.httpClient.get<any>("http://localhost:8080/api/user");
  }
  getAllUsersAvailabelForBoard(board_id){
    return this.httpClient.get<any>("http://localhost:8080/api/users/a/"+board_id);
  }
  deleteUserById(user_id){
    return this.httpClient.delete<any>("http://localhost:8080/api/user/"+user_id);
  }
  updateAUser(username,password,full_name,role,address,email,phone_number,department_id){
    return this.httpClient.put<any>("http://localhost:8080/api/user/"+department_id,{username,password,full_name,role,address,email,phone_number})
  }
  updateMembersInTheSameDepartment(users:User[]){
    this.membersInTheSameDepartmentSubject.next(users);
  }

  updateUserInEditView(user:User){
    this.userInEditViewSubject.next(user);
  }
}
