import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from '../model/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenService {

  constructor(private httpClient:HttpClient) { }
  authenticate(username,password){
    return this.httpClient.post<any>('http://localhost:8080/api/login',{username,password});
  }
  register(username,password,full_name,department_name,role,email,address,phone_number){
    return this.httpClient.post<any>("http://localhost:8080/api/register",{username,password,full_name,department_name,
      role,email,address,phone_number});
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    return !(user === null);
  }
  logOut() {
    sessionStorage.removeItem('username');
  }
}
