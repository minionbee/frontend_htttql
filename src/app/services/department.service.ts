import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Department } from '../model/Department.model';

@Injectable({
  providedIn:"root"
})

export class DepartmentService{
  departmentsSubject:BehaviorSubject<Department[]>=new BehaviorSubject<Department[]>(null);
  constructor(private httpClient:HttpClient){}
  setDepartmentSubject(department:Department[]){
    this.departmentsSubject.next(department);
  }
  getAllDepartment(){
    return this.httpClient.get<any>("http://localhost:8080/api/department");
  }
  getDepartmentById(department_id){
    return this.httpClient.get<any>("http://localhost:8080/api/department/"+department_id);
  }
  addANewDepartment(name,location){
    return this.httpClient.post<any>("http://localhost:8080/api/department",{name,location});
  }
  updateADepartment(id,name,location){
    return this.httpClient.put<any>("http://localhost:8080/api/department",{id,name,location});
  }
  deleteADepartment(department_id){
    return this.httpClient.delete<any>("http://localhost:8080/api/department/"+department_id);
  }
}
