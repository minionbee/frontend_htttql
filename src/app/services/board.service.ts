import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Board } from '../model/Board.model';
import { BehaviorSubject } from 'rxjs';
import { List } from '../model/List.model';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  boardSubject:BehaviorSubject<Board>=new BehaviorSubject<Board>(null);
  boardListSubject:BehaviorSubject<Board[]>=new BehaviorSubject<Board[]>(null);
  boardSearchListSubject:BehaviorSubject<Board[]>=new BehaviorSubject<Board[]>(null);

  constructor(private httpClient:HttpClient) { }

  // get a board
  getABoardById(id:string,username:string){
    return this.httpClient.get<any>('http://localhost:8080/api/board/'+id+"/"+username);
  }
  // get all boards by admin
  getAllBoardByAdmin(){
    return this.httpClient.get('http://localhost:8080/api/boards');
  }
  // get all boards by leader
  getAllBoardByLeader(username_leader:string){
    return this.httpClient.get<any>('http://localhost:8080/api/boards/'+username_leader);
  }
  // add board
  addNewBoard(username, name, created_date,budget,description){
    return this.httpClient.post<any>('http://localhost:8080/api/board/'+username,{name,created_date,budget,description});
  }
  // remove board
  deleteAnExistBoard(board_id){
    return this.httpClient.delete<any>('http://localhost:8080/api/board/'+board_id);
  }
  // update board
  updateAnExistBoard(id,name){
    return this.httpClient.put<any>('http://localhost:8080/api/board',{id,name});
  }


  //
  updateBoardListSubject(boards:Board[]){
    this.boardListSubject.next(boards);
  }
  //
  updateBoardsSearchSubject(boards:Board[]){
    this.boardSearchListSubject.next(boards);
  }
}
