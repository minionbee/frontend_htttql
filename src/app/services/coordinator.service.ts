import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
import { Coordinator } from '../model/Coordinator.model';

@Injectable({
  providedIn:"root"
})

export class CoordinatorService{
  coordinatorSubject:BehaviorSubject<Coordinator[]>=new BehaviorSubject<Coordinator[]>([]);
  constructor(private httpClient:HttpClient){}

  getAllBoardByUser(user_id){
    return this.httpClient.get<any>("http://localhost:8080/api/duty/user/"+user_id);
  }
  updateCoordinatorSubject(coordinators:Coordinator[]){
    this.coordinatorSubject.next(coordinators);
  }
}
