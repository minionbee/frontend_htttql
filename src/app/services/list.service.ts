import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private httpClient:HttpClient) { }

  addANewList(board_id,name,type){
    return this.httpClient.post<any>("http://localhost:8080/api/list/"+board_id,{name,type});
  }
}
