import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn:"root"
})

export class DetailService{

  constructor(private httpClient:HttpClient){}

  getAllDetailByTag(tag_id){
    return this.httpClient.get<any>("http://localhost:8080/api/detail/"+tag_id);
  }
  addADetail(name,tag_id,checklist_time){
    return this.httpClient.post<any>("http://localhost:8080/api/detail/"+tag_id,{name,checklist_time});
  }
  updateADetail(detail_id,status,name){
    return this.httpClient.put<any>("http://localhost:8080/api/detail/"+detail_id,{status,name});
  }
  deleteDetailById(detail_id){
    return this.httpClient.delete<any>("http://localhost:8080/api/detail/"+detail_id);
  }
}
