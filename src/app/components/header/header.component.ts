import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from "@angular/router";

import { CheckLoginService } from 'src/app/services/checklogin.service';
import { ControlLoginModalService } from 'src/app/services/control.login.modal.service';
import { ControlRegisterModalService } from 'src/app/services/control.register.modal.service';
import { BoardService } from 'src/app/services/board.service';
import { Board } from 'src/app/model/Board.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogined:boolean;
  isAdmin:boolean=false;
  username:string;

  board_keyword:string;

  boards:Board[];

  constructor(private checkLoginService:CheckLoginService,
    private controlLoginModalService:ControlLoginModalService,
    private controlRegisterModalService:ControlRegisterModalService,
    private router:Router, private boardService:BoardService) {
  }

  ngOnInit(): void {
    this.checkLoginService.loginStream$.subscribe(isLogin=>{
      if(isLogin===' ')this.isLogined=false;
      else this.isLogined=true;
    });
    this.checkLoginService.loginStream$.subscribe(user=>this.username=user);
    this.board_keyword="";
    this.boardService.boardListSubject.subscribe(boards=>this.boards=boards);
    if(sessionStorage.getItem("username")==="admin")this.isAdmin=true;
  }
  openLoginModal(){
    this.controlLoginModalService.setLoginModalStatus(true);
  }
  openRegisterModal(){
    this.controlRegisterModalService.setRegisterModalStatus(true);
  }
  logout(){
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("role");
    this.checkLoginService.updateLoginStatus(" ");
    this.router.navigate(['/logout']);
    // location.reload();
  }
  // search board
  search_board(){
    // console.log(keyword);
    let boards_search:Board[]=[];
    if(this.boards!==null || this.boards.length===0){
      for(let i=0;i<this.boards.length;i++)
        if(this.boards[i].name.toLowerCase().includes(this.board_keyword.toLowerCase()))
          boards_search.push(this.boards[i]);
      this.boardService.updateBoardsSearchSubject(boards_search);
    }
    // console.log(this.boards_search);
  }
}
