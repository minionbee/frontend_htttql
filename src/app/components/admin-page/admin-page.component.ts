import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/User.model';
import { UserService } from 'src/app/services/user.service';
import { ControlRegisterModalService } from 'src/app/services/control.register.modal.service';
import { DepartmentService } from 'src/app/services/department.service';
import { AuthenService } from 'src/app/services/authen.service';
import { Department } from 'src/app/model/Department.model';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  list_user:User[]=[];
  edited_user:User=new User("","","","","","","");
  edited_department:Department=new Department("","","");

  roles:string[]=['member','leader'];
  departments:Department[]=[];
  new_department:Department=new Department("","","");
  new_user:User=new User("","","","","","","");
  selected_department:number=0;

  open_edit_user_var:boolean;
  open_edit_dpt_var:boolean;
  dpt_var:boolean;
  user_var:boolean;

  add_dpt_var:boolean;
  add_user_var:boolean;
  config_user:any;
  config_dpt:any;
  constructor(private departmentService:DepartmentService,
    private userService:UserService, private authenService:AuthenService) { }

  ngOnInit(): void {
    this.open_edit_user_var=true;
    this.user_var=true;
    this.add_user_var=true;

    this.open_edit_dpt_var=false;
    this.dpt_var=false;
    this.add_dpt_var=false;

    this.departmentService.getAllDepartment().subscribe(departments=>{
      this.departments=departments;
    });
    this.userService.getAllUser().subscribe(users=>{
      // console.log(users);
      this.list_user=users;
    });
    this.config_user={
      itemsPerPage:5,
      currentPage:1,
      totalItems:this.list_user.length
    };
    this.config_dpt={
      itemsPerPage:5,
      currentPage:1,
      totalItems:this.departments.length
    };
  }
  // --------------------user--------------------
  pagination_user(page){
    this.config_user.currentPage=page;
  }
  open_edit_user_modal(username:string){
    this.userService.getUserByUsername(username).subscribe(user=>{
      console.log(user);
      this.edited_user=user;
    });
    this.user_var=false;
  }
  close_edit_user_modal(){
    this.user_var=true;
  }
  edit_user(edited_user:User){
    this.userService.updateAUser(edited_user.username,edited_user.password,
      edited_user.full_name,edited_user.role,edited_user.address,edited_user.email,edited_user.phone_number,this.selected_department).subscribe(user=>{
        this.update_list_user();
      })
    this.user_var=true;
  }
  delete_user(id:string,full_name:string){
    let delete_user=confirm(`Xóa người dùng: ${full_name}`);
    if(delete_user){
      this.userService.deleteUserById(id).subscribe(user=>{
        this.update_list_user();
      },error=>console.log(error));
      location.reload();
    }
  }
  control_user(){
    this.open_edit_user_var=true;
    this.add_user_var=true;
    this.user_var=true;
    //
    this.open_edit_dpt_var=false;
    this.add_dpt_var=false;
    this.dpt_var=false;
  }
  //add user
  control_add_user_modal(){
    this.add_user_var=!this.add_user_var;
  }
  add_user_fnc(user:User){
    this.authenService.register(user.username,user.password,user.full_name,this.selected_department,
      user.role,user.email,user.address,user.phone_number).subscribe(user=>{
        this.update_list_user();
      });
    this.control_add_user_modal();
  }
  // --------------------department--------------------
  pagination_dpt(page){
    this.config_dpt.currentPage=page;
  }
  open_edit_department_modal(department_id:string){
    this.departmentService.getDepartmentById(department_id).subscribe(department=>{
      console.log(department);
      this.edited_department=department;
    });
    this.dpt_var=false;
  }
  close_edit_department_modal(){
    this.dpt_var=true;
  }
  edit_department(edited_department:Department){
    this.departmentService.updateADepartment(edited_department.id,edited_department.name,edited_department.location).subscribe(
      department=>{
        this.update_list_department();
      });
    this.dpt_var=true;
  }
  delete_department(department_id:string,department_name:string){
    let delete_dpt=confirm(`Xóa phòng ban: ${department_name}`);
    if(delete_dpt){
      this.departmentService.deleteADepartment(department_id).subscribe(department=>{
        this.update_list_department();
        this.update_list_user();
      },error=>console.log(error));
      // location.reload();
    }
  }
  control_department(){
    this.open_edit_dpt_var=true;
    this.dpt_var=true;
    this.add_dpt_var=true;
    //
    this.open_edit_user_var=false;
    this.add_user_var=false;
    this.user_var=false;
  }

  // add department
  control_add_dpt_modal(){
    this.add_dpt_var=!this.add_dpt_var;
  }
  add_dpt_fnc(department:Department){
    this.departmentService.addANewDepartment(department.name,department.location).subscribe(
      department=>{
        console.log(department);
        this.update_list_department();
      }
    )
    this.control_add_dpt_modal();
  }
  //------------------------------------------------------------------------
  update_list_user(){
    this.userService.getAllUser().subscribe(users=>{
      this.list_user=users;
    });
  }
  update_list_department(){
    this.departmentService.getAllDepartment().subscribe(departments=>{
      this.departments=departments;
    });
  }
}
