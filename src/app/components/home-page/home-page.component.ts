import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Board } from 'src/app/model/Board.model';
import { BoardService } from 'src/app/services/board.service';
import { CheckLoginService } from 'src/app/services/checklogin.service';
import { CoordinatorService } from 'src/app/services/coordinator.service';
import { ControlLoginModalService } from 'src/app/services/control.login.modal.service';
import { ControlRegisterModalService } from 'src/app/services/control.register.modal.service';
import { Coordinator } from 'src/app/model/Coordinator.model';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/User.model';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  isLeader:boolean=false;
  isLogined:boolean;
  openAddBoardModal:boolean;
  boards:Board[];
  boards_search:Board[];
  name_board:string;
  budget_board:number;
  description_board:string;

  current_user:User=new User();
  url:string="";


  // coordinators:Coordinator[];
  constructor(private boardService:BoardService, private checkLoginService:CheckLoginService,
    private coordinatorService:CoordinatorService,
    private controlLoginModalService:ControlLoginModalService,
    private controlRegisterModalService:ControlRegisterModalService, private router:Router,
    private userService:UserService) { }

  ngOnInit(): void {
    this.boards=[];
    this.openAddBoardModal=false;
    this.checkLoginService.loginStream$.subscribe(isLogin=>{
      if(isLogin===' ')this.isLogined=false;
      else this.isLogined=true;
    });
    this.checkLoginService.loginStream$.subscribe(username=>{
      if(username.length!==1){
        this.userService.getUserByUsername(username).subscribe(user=>{
          this.current_user=user;
          if(user.role==='Division leader' || user.role==='Project manager')
            this.isLeader=true;
          this.coordinatorService.getAllBoardByUser(this.current_user.id).subscribe(coordinators=>{
            this.coordinatorService.updateCoordinatorSubject(coordinators);
          },error=>console.log(""));
        });
      }
    });

    this.coordinatorService.coordinatorSubject.subscribe(coordinators=>{
      // console.log(coordinators);
      let boards:Board[]=[];
      for(let i=0;i<coordinators.length;i++)
        boards.push(coordinators[i].boardDTO);
      if(boards!==null){
        this.boardService.updateBoardListSubject(boards);
      }
    },error=>console.log(error));
    this.boardService.boardListSubject.subscribe(boards=>{
      this.boards=boards;
    });
    this.boardService.boardSearchListSubject.subscribe(boards_search=>this.boards_search=boards_search);
    // this.update_list_board();
  }
  add_board_modal(){
    this.openAddBoardModal=true;
  }
  add_board_modal_close(){
    this.openAddBoardModal=false;
  }

  add_a_board(){
    let date:Date=new Date();
    //console.log(date.toJSON());
    this.boardService.addNewBoard(sessionStorage.getItem('username'),this.name_board,date.toJSON().split(".")[0],
                                    this.budget_board,this.description_board).subscribe(
      board=>{
        this.boards.push(board);
      }
    );
    location.reload();
    this.openAddBoardModal=false;
  }
  delete_board(board_id:string){
    let confirmed=confirm("Bạn có muốn xóa dự án này ko");
    if(confirmed){
      this.boardService.deleteAnExistBoard(board_id).subscribe(board=>console.log(board),error=>console.log(error));
      this.update_list_board();
      location.reload();
    }
  }
  login_to_view(){
    this.controlLoginModalService.setLoginModalStatus(true);
  }
  register_to_view(){
    this.controlRegisterModalService.setRegisterModalStatus(true);
  }

  update_list_board(){
    // get all coordinator after login
    if(sessionStorage.getItem("username")!=="" && sessionStorage.getItem("username")!==null)
    this.coordinatorService.getAllBoardByUser(this.current_user.id).subscribe(coordinators=>{
      this.coordinatorService.updateCoordinatorSubject(coordinators);
    },error=>console.log("Hãy đăng nhập đi"));
    // set all boards from coordinators json save to Observable board
    this.coordinatorService.coordinatorSubject.subscribe(coordinators=>{
      let boards:Board[]=[];
      for(let i=0;i<coordinators.length;i++)
        {
          boards.push(coordinators[i].boardDTO);
          this.boardService.updateBoardListSubject(boards);
        };
    },error=>console.log(error));
  }
}
