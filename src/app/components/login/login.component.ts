import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CheckLoginService } from 'src/app/services/checklogin.service';
import { ControlLoginModalService } from 'src/app/services/control.login.modal.service';
import { AuthenService } from 'src/app/services/authen.service';
import { CoordinatorService } from 'src/app/services/coordinator.service';
import { Board } from 'src/app/model/Board.model';
import { BoardService } from 'src/app/services/board.service';
import { User } from 'src/app/model/User.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username:string="";
  password:string;
  openLoginModal:boolean;
  constructor(private checkLoginService:CheckLoginService,
    private controlLoginModalService:ControlLoginModalService,
    private authenService:AuthenService, private coordinatorService:CoordinatorService,
    private boardService:BoardService, private router: Router) { }

  ngOnInit(): void {
    this.controlLoginModalService.loginModalStatus$.subscribe(status=>{
      this.openLoginModal=status;
    });
  }
  login(){
    this.authenService.authenticate(this.username,this.password).subscribe(userData=>{
      sessionStorage.setItem('username',this.username);
      sessionStorage.setItem('role',userData.role);
      let user:User=userData;
      if(this.username!=="admin"){
        this.coordinatorService.getAllBoardByUser(user.id).subscribe(coordinators=>{
          this.coordinatorService.updateCoordinatorSubject(coordinators);
        },error=>console.log(""));
        this.coordinatorService.coordinatorSubject.subscribe(coordinators=>{
          let boards_old:Board[]=[];
          let boards_new:Board[]=[];
          for(let i=0;i<coordinators.length;i++)
            {
              boards_old.push(coordinators[i].boardDTO);
            };
          if(boards_old!==null){
            for(let i=0;i<boards_old.length;i++){
              let board:Board;
              this.boardService.getABoardById(boards_old[i].id,"null").subscribe(board=>{
                boards_new.push(board);
              });

            };
          }
          this.boardService.updateBoardListSubject(boards_new);
          // console.log(boards_new);
        },error=>console.log(error));
      }
      this.checkLoginService.updateLoginStatus(this.username);
      this.controlLoginModalService.setLoginModalStatus(false);
      this.username="";
      this.password="";
      if(sessionStorage.getItem("username")!=="admin")
        this.router.navigate(['/homepage']);
        // location.reload();
      else this.router.navigate(['/admin']);
    },error=>{
      alert("Sai tài khoản hoặc mật khẩu");
      return
    });

  }
  cancelLoginProcess(){
    this.controlLoginModalService.setLoginModalStatus(false);
  }
}
