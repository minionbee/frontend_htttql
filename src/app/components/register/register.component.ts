import { Component, OnInit } from '@angular/core';
import { ControlRegisterModalService } from 'src/app/services/control.register.modal.service';
import { DepartmentService } from 'src/app/services/department.service';
import { Department } from 'src/app/model/Department.model';
import { User } from 'src/app/model/User.model';
import { AuthenService } from 'src/app/services/authen.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  openRegisterModal:boolean;
  // username:string;
  // password:string;
  // email:string;
  // address:string;
  // full_name:string;
  // role:string;
  user:User=new User("","","","","","","");

  selected_department:number;
  department_id:number[];
  roles:string[]=['Developer','Designer','Project manager','Technical leader','Division leader','Tester','Administrator'];
  departments:Department[];

  register_update:string;
  constructor(private controlRegisterModalService:ControlRegisterModalService,
    private departmentService:DepartmentService,
    private authenticateService:AuthenService,
    private userService:UserService) { }

  ngOnInit(): void {
    this.controlRegisterModalService.registerModalStatus$.subscribe(status=>this.openRegisterModal=status);
    this.departmentService.getAllDepartment().subscribe(departments=>{
      this.departments=departments;
    });
    if(sessionStorage.getItem("username")===null){
      let new_user:User=new User("","","","","","","","");
      this.register_update="Đăng ký";
      this.userService.updateUserInEditView(new_user);
    }
    else {
      this.userService.getUserByUsername(sessionStorage.getItem("username")).subscribe(user=>{
        this.userService.updateUserInEditView(user);
        // console.log(this.user)
      });
      this.register_update="Cập nhật thông tin tài khoản";
    }
    this.userService.userInEditViewSubject.subscribe(user=>this.user=user);
  }
  register(){
    if(this.register_update==="Đăng ký"){
      // console.log(this.departments[this.selected_department].name);
      this.authenticateService.register(this.user.username,this.user.password,this.user.full_name,this.selected_department,
        this.user.role,this.user.email,this.user.address,this.user.phone_number).subscribe(user=>{
          this.refresh();
        });
    }
    else {
      this.userService.updateAUser(this.user.username,this.user.password,this.user.full_name,
        this.user.role,this.user.address,this.user.email,
          this.user.phone_number,this.selected_department).subscribe(user=>{
            this.userService.updateUserInEditView(user);
          })
    }

    this.controlRegisterModalService.setRegisterModalStatus(false);
  }
  closeRegisterModal(){
    this.controlRegisterModalService.setRegisterModalStatus(false);
  }
  refresh(){
    this.user.username="";
    this.user.password="";
    this.user.full_name="";
    this.user.role="member";
    this.user.email="";
    this.user.address="";
    this.user.phone_number="";
  }
}
