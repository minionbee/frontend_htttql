import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/User.model';
import { Department } from 'src/app/model/Department.model';
import { DepartmentService } from 'src/app/services/department.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  openUpdateUserModal:boolean;
  user:User;
  departments:Department[];
  roles:string[]=['member','leader'];
  selected_department:string="A";
  constructor(private userService:UserService,
    private departmentService:DepartmentService) { }

  ngOnInit(): void {
    this.userService.getUserByUsername(sessionStorage.getItem("username")).subscribe(user=>{
      this.user=user;
    });
    this.departmentService.getAllDepartment().subscribe(departments=>{
      this.departments=departments;
    })
  }
  updateUser(){

  }
  close_update_user_modal(){

  }
}
