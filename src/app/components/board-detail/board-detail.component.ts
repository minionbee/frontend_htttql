import { Component, OnInit, forwardRef } from '@angular/core';
import { CdkDrag, CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ActivatedRoute,ParamMap } from "@angular/router";
import { BoardService } from 'src/app/services/board.service';
import { Board } from 'src/app/model/Board.model';
import { List } from 'src/app/model/List.model';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/User.model';
import { TagService } from 'src/app/services/tag.service';
import { Tag } from 'src/app/model/Tag.model';
import { DetailService } from 'src/app/services/detail.service';
import { Detail } from 'src/app/model/Detail.model';
import { Debate } from 'src/app/model/Debate.model';
import { DebateService } from 'src/app/services/debate.service';
import { ListService } from 'src/app/services/list.service';
import { Coordinator } from 'src/app/model/Coordinator.model';
import { DutyService } from 'src/app/services/duty.service';

@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.css']
})
export class BoardDetailComponent implements OnInit{
  isLeader:boolean=false;
  is_attendee:boolean=false;
  openModalEditTag:boolean;
  openModalEditNameTag:boolean;
  openModalAddTag:boolean;
  openModalAddDetail:boolean;
  openModalAddMember:boolean;
  openModalAddMemberm:boolean;
  openModalAddLabel:boolean;
  addMemberToBoardModal:boolean;

  add_detail_btn:boolean;
  add_list_modal:boolean;
  add_list_btn:boolean;

  detail_edited_id:any="";

  board:Board;
  lists:List[];
  // members:User[]=[];
  users_to_created_card:User[]=[];
  users_to_updated_card:User[]=[];
  users_to_update_board:User[]=[];
  debates:Debate[]=[];
  details:Detail[]=[];
  duties:Coordinator[]=[];

  member:User=new User("","","","","","","");
  name_tag:string;
  name_detail:string="";
  name_detail_change:string="";

  label:string="not important";
  is_important:string;
  name_tag_change:string;
  selected_list_id:string="";
  label_important:string[]=["important","not important"];

  tag:Tag=new Tag("","");
  list:List=new List("","");


  check:number;

  start_date:Date;
  end_date:Date;
  checklist_time:Date;
  time_now:Date;

  new_name_board:string="";
  openModalEditNameBoard:boolean;
  // percent_complete:number=0;

  comment_content:string;

  current_username:string;
  current_user:User;

  created_tag_users:User[]=[];

  new_name_list:string;
  constructor(private route:ActivatedRoute,
    private boardService:BoardService, private userService:UserService,
    private tagService:TagService, private detailService:DetailService,
    private debateService:DebateService, private listService:ListService,
    private dutyService:DutyService) { }

  ngOnInit(): void {
    this.add_detail_btn=true;
    this.openModalEditTag=false;
    this.openModalAddTag=false;
    this.openModalAddDetail=false;
    this.openModalAddMember=false;
    this.openModalAddLabel=false;
    this.openModalAddMemberm=false;
    this.openModalEditNameTag=false;
    this.openModalEditNameBoard=false;
    this.addMemberToBoardModal=false;
    this.add_list_modal=false;
    this.add_list_btn=false;
    this.check=1;
    this.time_now=new Date();
    this.current_username=sessionStorage.getItem("username");
    this.userService.getUserByUsername(sessionStorage.getItem("username")).subscribe(current_user=>{
      this.current_user=current_user;
      if(this.current_user.role==='Project manager' || this.current_user.role==='Division leader')
      {
        this.isLeader=true;
        this.is_attendee=true;
      }
      this.userService.getAllUsersAvailabelForBoard(window.location.href.toString().split("/")[4]).subscribe(users=>this.userService.updateMembersInTheSameDepartment(users));
      this.update_main_view();
    });
  }
  // -------------------------------------board-------------------------------------
  control_edit_name_board_modal(){
    this.new_name_board="";
    this.openModalEditNameBoard=!this.openModalEditNameBoard;
  }
  update_name_board(){
    this.boardService.updateAnExistBoard(this.board.id,this.new_name_board).subscribe(
      board=>{
        this.board=board;
        this.control_edit_name_board_modal();
      }
    )
  }
  // -------------------------------------tag-------------------------------------
  open_edit_tag_modal(selected_tag:Tag){
    this.tag=selected_tag;
    this.list=this.tag.listBoard;
    if(this.current_user.role==='Project manager' || this.current_user.role==='Division leader')
    {
      this.is_attendee=true;
    }
    if(this.tag.attendees.length>0){
      for(let attendee of this.tag.attendees){
        if(attendee.user.id===this.current_user.id)
        {
          this.is_attendee=true;
          break;
        }
      }
    }
    this.detailService.getAllDetailByTag(this.tag.id).subscribe(details=>{
      this.details=details;
    });
    this.debateService.getAllDebateByCard(this.tag.id).subscribe(debates=>{
      this.debates=debates;
      this.openModalEditTag=true;
    });
  }
  close_edit_tag_modal(){
    this.tag=new Tag();
    this.openModalEditTag=false;
  }
  // move_between_list(tag:Tag){
  //   this.tagService.updateATag(tag.id,tag.name,tag.label).subscribe(tag=>{
  //   });
  //   location.reload();
  // }
  add_new_tag(list_id){
    let tag_created:Tag;
    if(this.created_tag_users.length===null||this.created_tag_users.length===0){
      this.tagService.addNewTag(list_id,null,this.name_tag,this.label,
        this.start_date.toJSON().split(".")[0],this.end_date.toJSON().split(".")[0]).subscribe(
        tag=>{
          if(tag.label==="important") tag.label="crimson";
          this.update_main_view();
          this.name_tag="";
          this.member=new User("","","","","","","");
        },
        error=>console.log(error)
      )
    }
    else if(this.created_tag_users.length===1){
      this.tagService.addNewTag(list_id,this.created_tag_users[0].id,this.name_tag,this.label,
        this.start_date.toJSON().split(".")[0],this.end_date.toJSON().split(".")[0]).subscribe(
        tag=>{
          if(tag.label==="important") tag.label="crimson";
          this.update_main_view();
          this.name_tag="";
          this.member=new User("","","","","","","");
        },
        error=>console.log(error)
      )
    }
    else if(this.created_tag_users.length>1){
      this.tagService.addNewTag(list_id,this.created_tag_users[0].id,this.name_tag,this.label,
        this.start_date.toJSON().split(".")[0],this.end_date.toJSON().split(".")[0]).subscribe(
        tag=>{
          console.log(tag);
          tag_created=tag;
          for(let i=1;i<this.created_tag_users.length;i++){
            this.tagService.updateUserToTag(tag_created.id,1,this.created_tag_users[i].id);
          }
          this.update_main_view();
              this.name_tag="";
        },
        error=>console.log(error)
      )
    }
    this.control_add_tag_modal(null);
    //console.log(this.check);
  }
  add_member_to_tag(member:User){
    this.member=member;
    if(member!==null){
      for(let i=0;i<this.users_to_created_card.length;i++){
        if(this.users_to_created_card[i].id===member.id && this.users_to_created_card[i].selected_user_in_created_card===0){
          this.users_to_created_card[i].selected_user_in_created_card=1;
          this.created_tag_users.push(member);
          break;
        }
        else if(this.users_to_created_card[i].id===member.id && this.users_to_created_card[i].selected_user_in_created_card===1){
          this.users_to_created_card[i].selected_user_in_created_card=0;
          for(let j=0;j<this.created_tag_users.length;j++){
            if(this.created_tag_users[j].id===member.id){
              this.created_tag_users.splice(j,1);
              break;
            }
          }
          break;
        }
      }
    }
    // console.log(this.member);
    // this.control_add_memberm_modal();
  }
  add_label_to_tag(){
    this.check+=1;
    if(this.check%2===0){
      this.label="important";
    }
    else this.label="not important";
  }
  update_member_to_tag(member:User){
    // console.log(this.tag);
    if(member.selected_user_in_created_card===1){
      this.tagService.updateUserToTag(this.tag.id,0,member.id).subscribe(tag=>{
        // console.log(tag);
        this.tag=tag;
        this.update_main_view();
      },error=>console.log(error));
    }
    else{
      this.tagService.updateUserToTag(this.tag.id,1,member.id).subscribe(tag=>{
        this.tag=tag;
        this.update_main_view();
      },error=>console.log(error));
    }
    this.control_add_member_modal(null);
  }
  update_label_to_tag(label:string){
    this.tagService.updateATag(this.tag.id,null,2,this.tag.name,label).subscribe(tag=>{
      // console.log(tag);
      this.tag=tag;
      this.update_main_view();
    },error=>console.log(error));
    this.control_add_label_modal();
  }
  update_tag(tag:Tag){
    this.tagService.updateATag(this.tag.id,null,2,this.name_tag_change,this.tag.label).subscribe(tag=>{
      // console.log(tag);
      this.tag=tag;
      this.update_main_view();
    },error=>console.log(error));
    this.openModalEditNameTag=false;
    this.name_tag_change="";
    this.openModalEditNameTag=false;
  }
  control_add_tag_modal(list_id:string){
    this.selected_list_id=list_id;
    this.created_tag_users=[];
    for(let i=0;i<this.users_to_created_card.length;i++)
      this.users_to_created_card[i].selected_user_in_created_card=0;
    this.openModalAddTag=!this.openModalAddTag;
  }
  control_add_member_modal(card:Tag){
    if(card!==null){
      for(let i=0;i<this.users_to_updated_card.length;i++){
        this.users_to_updated_card[i].selected_user_in_created_card=0;
        for(let j=0;j<card.attendees.length;j++){
          if(card.attendees[j].user.id===this.users_to_updated_card[i].id)
            this.users_to_updated_card[i].selected_user_in_created_card=1;
        }
      }
    }
    this.openModalAddMember=!this.openModalAddMember;
  }
  control_add_label_modal(){
    this.openModalAddLabel=!this.openModalAddLabel;
  }
  control_add_memberm_modal(){
    this.openModalAddMemberm=!this.openModalAddMemberm;
  }
  control_edit_name_tag_modal(){
    this.openModalEditNameTag=!this.openModalEditNameTag;
  }
  update_main_view(){
    this.userService.membersInTheSameDepartment$.subscribe(users=>{
      let user_in_department:User[]=users;
      this.users_to_update_board=[];
      if(user_in_department!==null){
        let member:User=new User();
        for(member of user_in_department){
          this.users_to_update_board.push(member);
        }
      }
    });
    this.route.paramMap.subscribe((params:ParamMap)=>{
      this.boardService.getABoardById(params.get('id'),params.get("username")).subscribe(board=>{
        this.boardService.boardSubject.next(board);
        this.dutyService.getAllMemberCurrentInABoard(board.id).subscribe(duties=>{
          this.duties = duties;
          let duty:Coordinator;
          this.users_to_updated_card=[];
          this.users_to_created_card=[];
          for(duty of this.duties){
            let member:User = duty.userDTO;
            member.selected_user_in_created_card=0;
            this.users_to_created_card.push(member);
            this.users_to_updated_card.push(member);
            //

          }
        });
        this.board=board;
        this.lists=this.board.lists;
        // sort cards in a list
        let list:List;
        for(list of this.lists){
          list.cardDTOS.sort((o1:Tag,o2:Tag)=>o1.isort-o2.isort);
          if(list.cardDTOS.length!==0 || list.cardDTOS.length!==null)
            this.update_percent_complete_in_main_view(list.cardDTOS);
        }
      })
    });

  }
  // -------------------------------------detail-------------------------------------
  add_a_detail(){
    this.detailService.addADetail(this.name_detail,this.tag.id,this.checklist_time.toJSON().split(".")[0]).subscribe(detail=>{
        this.detail_update(detail);
      }
    );
    this.name_detail="";
    this.control_add_detail_modal();
  }
  delete_detail(id:string){
    let is_confirm=confirm("Xác nhận xóa?");
    if(is_confirm){
      this.detailService.deleteDetailById(id).subscribe(detail=>{
        this.detail_update(detail);
      },error=>console.log(error));
    }
  }
  edit_detail(id:string,status:string,check:string){
    let cpl:string;
    if(check==='0'){
      if(status==="complete")cpl="doing";
      else cpl="complete";
    }
    else cpl=status;
    this.detailService.updateADetail(id,cpl,this.name_detail_change).subscribe(detail=>{
      this.detail_update(detail);
    });
    this.name_detail_change="";
    this.detail_edited_id="";
    // location.reload();
  }
  detail_update(detail){
    // console.log(detail);
    this.detailService.getAllDetailByTag(this.tag.id).subscribe(details=>{
      this.details=details;
    });
    this.update_tag_list();
  }
  // modal detail
  edit_detail_form(id:string){
    this.detail_edited_id=id;
  }
  close_update_detail_modal(){
    this.detail_edited_id="";
  }
  control_add_detail_modal(){
    // if(!this.openModalAddDetail)this.add_detail_btn=false;
    // else this.add_detail_btn=true;
    this.openModalAddDetail=!this.openModalAddDetail;
  }
  // add deadline time
  add_deadline_time(deadline_time){
    this.start_date=new Date();
    this.end_date=new Date(Date.parse(deadline_time));
    //console.log(this.start_date.+" "+this.end_date);
  }
  add_time_checklist(checklist_time){
    this.checklist_time = new Date(Date.parse(checklist_time));
  }
  deleteATag(id:string,name_tag:string){
    let confirm_delete=confirm(`Xác nhận xóa: ${name_tag}`);
    if(confirm_delete){
      // console.log(id)
      this.tagService.removeATag(id).subscribe(tag=>{
        this.update_tag_list();
      },error=>console.log(error));
      // location.reload();
    }
  }
  // general function
  update_tag_list(){
    this.route.paramMap.subscribe((params:ParamMap)=>{
      this.boardService.getABoardById(params.get('id'),params.get("username")).subscribe(board=>{
        this.boardService.boardSubject.next(board);
        // console.log(board);
        this.board=board
        this.lists=this.board.lists;
        // sort cards in a list
        let list:List;
        for(list of this.lists){
          // console.log(list);
          // list.tags.sort((o1:Tag,o2:Tag)=>o1.isort-o2.isort);
          if(list.cardDTOS.length!==0 || list.cardDTOS.length!==null)
            this.update_percent_complete_in_main_view(list.cardDTOS);
        }
        if(this.openModalEditTag){
          // for(let i=0;i<this.todoList.length;i++)
          //   if(this.tag.id===this.todoList[i].id){
          //     this.tag=this.todoList[i];
          //     break;
          //   }
          let list_b:List;
          for(list of this.lists){
            for(let card of list.cardDTOS){
              if(this.tag.id===card.id){
                this.tag=card;
                break;
              }
            }
          }
        }
      },error=>console.log(error));
    });
  }
  update_percent_complete_in_main_view(tags:Tag[]){
    for(let i=0;i<tags.length;i++){
      tags[i].qty=0;
      // if(tags[i].label==='important')tags[i].label="crimson";
      // console.log(tags);
      for(let j=0;j<tags[i].checkList.length;j++)
      {
        // console.log(tags[i]);
        if(tags[i].checkList[j].status==='complete'){
          tags[i].qty+=1;
        }
      }
    }
  }
  post_comment(tag:Tag){
    let date:Date=new Date();
    this.debateService.postAComment(this.comment_content,date.toJSON().split(".")[0],sessionStorage.getItem("username"),tag.id).subscribe(comment=>{
      this.debateService.getAllDebateByCard(tag.id).subscribe(debates=>{
        this.debates=debates;
        this.comment_content="";
      });
    });
  }
  delete_a_comment(deleted_debate:Debate,tag:Tag){
    this.debateService.deleteAComment(deleted_debate.id).subscribe(deleted_debate=>{
      this.debateService.getAllDebateByCard(tag.id).subscribe(debates=>{
        this.debates=debates;
      });
    });
  }
  drop(event: CdkDragDrop<string[]>){
    let pre_index = event.previousIndex + 1;
    let new_index = event.currentIndex + 1;
    let list:List=JSON.parse(JSON.stringify(event.previousContainer.data));
    if (event.previousContainer === event.container) {
      if(event.previousIndex !== event.currentIndex){
        this.tagService.moveLocationTag(list.cardDTOS[event.previousIndex].id,event.previousContainer.id,pre_index,new_index).subscribe(cards=>{
          this.update_main_view();
        });
      }
    } else {
      this.tagService.moveTagBetweenTwoList(list.cardDTOS[event.previousIndex].id,event.previousContainer.id,event.container.id,pre_index,new_index).subscribe(cards=>{
        this.update_main_view();
      });
    }
  }

  //
  add_a_new_list(name:string){
    this.listService.addANewList(this.board.id,name,"").subscribe(list=>{
      this.lists.push(list);
      this.new_name_list="";
      this.add_list_modal=false;
      this.add_list_btn=false;
    });
  }
  open_add_list_modal(){
    this.add_list_modal=true;
    this.add_list_btn=true;
  }
  cancel_add_list(){
    this.add_list_modal=false;
    this.add_list_btn=false;
  }
  open_add_member_to_board_modal(){
    this.addMemberToBoardModal=true;
  }
  add_member_to_board(user_id:string,is_add:any){
    if(is_add===1){
      this.dutyService.deleteADuty(window.location.href.toString().split("/")[4],user_id).subscribe(duty=>{
        this.userService.getUserByUsername(sessionStorage.getItem("username")).subscribe(current_user=>{
          this.current_user=current_user;
          if(this.current_user.role==='Project manager' || this.current_user.role==='Division leader')
            this.isLeader=true;
          this.userService.getAllUsersAvailabelForBoard(window.location.href.toString().split("/")[4]).subscribe(users=>this.userService.updateMembersInTheSameDepartment(users));
          this.update_main_view();
        });
      });
    }
    else if(is_add===0){
      this.dutyService.addANewMemberToBoard(window.location.href.toString().split("/")[4],user_id,this.current_user.role).subscribe(duty=>{
        this.userService.getUserByUsername(sessionStorage.getItem("username")).subscribe(current_user=>{
          this.current_user=current_user;
          if(this.current_user.role==='Project manager' || this.current_user.role==='Division leader')
            this.isLeader=true;
          this.userService.getAllUsersAvailabelForBoard(window.location.href.toString().split("/")[4]).subscribe(users=>this.userService.updateMembersInTheSameDepartment(users));
          this.update_main_view();
        });
      });
    }
    this.addMemberToBoardModal=false;
  }
  close_add_member_to_board_modal(){
    this.addMemberToBoardModal=false;
  }
}


