import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from "ngx-pagination";
import { DragDropModule } from '@angular/cdk/drag-drop';

import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { BoardDetailComponent } from './components/board-detail/board-detail.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { RegisterComponent } from './components/register/register.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';


const routes: Routes = [
  {path:'', component:HomePageComponent},
  {path:'homepage', component:HomePageComponent},
  {path:'board/:id/:username',component:BoardDetailComponent},
  {path:'admin',component:AdminPageComponent},
  {path:'logout',component:HomePageComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
    CommonModule,
    HttpClientModule,
    NgxPaginationModule,
    DragDropModule
  ],
  declarations:[
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    UpdateUserComponent,
    HomePageComponent,
    BoardDetailComponent,
    AdminPageComponent
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
